<?php

use Illuminate\Database\Seeder;

class EnumSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitySeeder::class);
        $this->call(ClubTypeSeeder::class);
        $this->call(MusicGenresSeeder::class);
    }
}
