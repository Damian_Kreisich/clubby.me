<?php

use Illuminate\Database\Seeder;
use App\Review;

class ReviewSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Review::create([
            'ClubID' => 4,
            "AppUserID" => 1,
            "Content" => "Ludzie, klimat, miejsce 5* !!! Nowo, obowiązkowe miejsce przy każdej wizycie we Wrocławiu",
            "Factor1" => 4,
            "Factor2" => 4,
            "Factor3" => 4,
            "Factor4" => 5,
            "Overall" => 4,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 4,
            "AppUserID" => 2,
            "Content" => "Pierwszy nasz koncert w stolicy Dolnego Śląska, a mój drugi, bo kiedyś tam ponad ćwierć wieku temu zdarzyło się mi gdzieś na Muchoborze z Latające Odchody. Wieści mam super - poznałem super ludzi !!!",
            "Factor1" => 4,
            "Factor2" => 4,
            "Factor3" => 4,
            "Factor4" => 5,
            "Overall" => 4,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 4,
            "AppUserID" => 3,
            "Content" => "Można tu przyjść na koncert, można przyjść zagrać koncert, można przyjść posiedzieć przy barze albo strzelić pyszną pigwówkę :) wszystko z tego sprawdzilam i polecam! jedyne prawdziwe metalowe miejsce we Wro.",
            "Factor1" => 4,
            "Factor2" => 4,
            "Factor3" => 4,
            "Factor4" => 5,
            "Overall" => 4,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 1,
            "AppUserID" => 1,
            "Content" => "Ludzie, klimat, miejsce 5* !!! Nowo, obowiązkowe miejsce przy każdej wizycie we Wrocławiu",
            "Factor1" => 4,
            "Factor2" => 4,
            "Factor3" => 4,
            "Factor4" => 5,
            "Overall" => 4,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 2,
            "AppUserID" => 1,
            "Content" => "Ludzie, klimat, miejsce 5* !!! Nowo, obowiązkowe miejsce przy każdej wizycie we Wrocławiu",
            "Factor1" => 4,
            "Factor2" => 4,
            "Factor3" => 4,
            "Factor4" => 5,
            "Overall" => 4,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 3,
            "AppUserID" => 1,
            "Content" => "Ludzie, klimat, miejsce 5* !!! Nowo, obowiązkowe miejsce przy każdej wizycie we Wrocławiu",
            "Factor1" => 4,
            "Factor2" => 4,
            "Factor3" => 4,
            "Factor4" => 5,
            "Overall" => 4,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 5,
            "AppUserID" => 1,
            "Content" => "Ludzie, klimat, miejsce 5* !!! Nowo, obowiązkowe miejsce przy każdej wizycie w Katowicach",
            "Factor1" => 4,
            "Factor2" => 5,
            "Factor3" => 5,
            "Factor4" => 5,
            "Overall" => 5,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'ClubID' => 6,
            "AppUserID" => 1,
            "Content" => "Ludzie, klimat, miejsce 5* !!! Nowo, obowiązkowe miejsce przy każdej wizycie w Gdansku",
            "Factor1" => 4,
            "Factor2" => 5,
            "Factor3" => 5,
            "Factor4" => 5,
            "Overall" => 5,
            "Discriminator" => "ClubReview"
        ]);

        Review::create([
            'EventID' => 1,
            "AppUserID" => 1,
            "Content" => "Cudowne wydarzenie, najlepsze wspomnienia jakie mogłem sobie wymarzyć, dziękuję za świetną organizację!",
            "Factor1" => 5,
            "Factor2" => 5,
            "Factor3" => 5,
            "Factor4" => 5,
            "Overall" => 5,
            "Discriminator" => "EventReview"
        ]);

        Review::create([
            'EventID' => 1,
            "AppUserID" => 2,
            "Content" => "Czekamy na kolejną edycję wydarzenia! Byle jak najszybciej!",
            "Factor1" => 5,
            "Factor2" => 5,
            "Factor3" => 5,
            "Factor4" => 5,
            "Overall" => 5,
            "Discriminator" => "EventReview"
        ]);
    }
}
