<?php

use Illuminate\Database\Seeder;

class ClubSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        require "SeedImage.php";

        DB::table('Club')->insert([
            "SiteStatsID" => null,
            "ClubTypeID" => 1,
            "CityID" => 1,
            "AppUserID" => 1,
            "Name" => "Ciemna strona miasta",
            "Address" => "Plac Wroblewskiego 15",
            "FbContentURL" => "https://www.facebook.com/CiemnaStronaMiasta",
            "ShortDesc" => "Najlepszy najmroczniejszy klub muzyczny we Wroclawiu w klimacie: metal, core, gothic i hard rock. Koncerty, imprezy, czeskie piwo, free WIFI.",
            "Logo" => $csmLogo,
            "AboutUsContent" => "Ciemna Strona Miasta to klub muzyczny dla fanow ciezkich brzmien i mrocznych klimatow. Rzadzi tam calkowicie ostre granie spod znaku metal, hardcore, gothic, hard rock na zywo (koncerty) i z glosnikow. W CSM tylko ciezkie granie i nie ma zmiluj. Jedyny taki klub, ktory sie nie zmienia muzycznie zaleznie od imprezy. Ciemna Strona Miasta to projekt trzech przyjaciol, ktorzy poznali sie i wspoltworzyli kultowe miejsce spotkan metalowcow w latach 90 poprzedniego wieku – SWOBODNA (sklep muzyczny w szesciokatnym baraku na ul. Swobodnej). Wspolne odkrywanie swiata muzyki, zabawy i doroslego zycia z przed ponad dwudziestu laty zaowocowaly teraz stworzeniem Klubu Muzycznego Ciemna Strona Miasta. To miejsce, ktore ma polaczyc przeszlosc ciezkiej muzyki, lata 80 i 90 zyjace w naszych mlodych wspomnieniach, oraz chwile obecna, kiedy metal, core, gothic staly sie ogolnie uznanymi gatunkami muzycznymi. To miejsce, w ktorym marzenia opowiadane po kolejnym piwie, staly sie rzeczywistoscia, choc wiele lat pozniej. To miejsce, w ktorym troche starsi i troche mlodsi wielbiciele ciezkich i mrocznych brzmien moga zasiasc przy stole, zeby posluchac ciezkiej muzyki. To miejsce, ktore robimy tak samo dla was jak i dla siebie, w takiej przestrzeni chcielibysmy przebywac. To miejsce, ktore bedzie podstawa stworzenia i zintegrowania ludzi ciezkiej muzy, czyli armii Ciemnej Strony Miasta. To miejsce, w ktorym zabawa ma sie mieszac ze sztuka, mroczna i klimatyczna. To miejsce, dla Was, dla Nas, dla wszystkich, ktorzy lubia taki klimat.",
            "AboutUsPhoto1" => $imagePlaceholder,
            "AboutUsPhoto2" => $imagePlaceholder,
            "AboutUsPhoto1Alt" => "alt",
            "AboutUsPhoto2Alt" => "alt",
            "OverallRating" => null,
            "OfferRating" => null,
            "PricesRating" => null,
            "SoundRating" => null,
            "AtmRating" => null
        ]);
        DB::table('Club')->insert([
            "SiteStatsID" => null,
            "ClubTypeID" => 2,
            "CityID" => 1,
            "AppUserID" => 1,
            "Name" => "Alive",
            "Address" => "Nasyp 17",
            "FbContentURL" => "https://www.facebook.com/KlubMuzycznyAlive",
            "ShortDesc" => "Best Club EVER",
            "Logo" => $aliveLogo,
            "AboutUsContent" => "AboutUs",
            "AboutUsPhoto1" => $imagePlaceholder,
            "AboutUsPhoto2" => $imagePlaceholder,
            "AboutUsPhoto1Alt" => "alt",
            "AboutUsPhoto2Alt" => "alt",
            "OverallRating" => null,
            "OfferRating" => null,
            "PricesRating" => null,
            "SoundRating" => null,
            "AtmRating" => null
        ]);
        DB::table('Club')->insert([
            "SiteStatsID" => null,
            "ClubTypeID" => 1,
            "CityID" => 1,
            "AppUserID" => 1,
            "Name" => "Od Zmierzchu do Switu",
            "Address" => "ul.Krupnicza 15",
            "FbContentURL" => "https://www.facebook.com/Od-Zmierzchu-Do-%C5%9Awitu-oficjany-profil-131047210328703",
            "ShortDesc" => "KLUB OD ZMIERZCHU DO SWITU JEST IKONa MUZYKI ROCKOWEJ W MIEsCIE WROClAW. ISTNIEJE JUZ PONAD 12 LAT I NADAL MA SIe DOBRZE. ODBYWAJa SIe TUTAJ NIEZAPOMNIANE KONCERTY PRZERozNYCH KAPEL, ZARoWNO Z POLSKI, JAK I ZZA GRANICY.",
            "Logo" => $zmierchLogo,
            "AboutUsContent" => "AboutUs",
            "AboutUsPhoto1" => $imagePlaceholder,
            "AboutUsPhoto2" => $imagePlaceholder,
            "AboutUsPhoto1Alt" => "alt",
            "AboutUsPhoto2Alt" => "alt",
            "OverallRating" => null,
            "OfferRating" => null,
            "PricesRating" => null,
            "SoundRating" => null,
            "AtmRating" => null
        ]);
        DB::table('Club')->insert([
            "SiteStatsID" => null,
            "ClubTypeID" => 3,
            "CityID" => 1,
            "AppUserID" => 1,
            "Name" => "A2",
            "Address" => "ul. Goralska 5",
            "FbContentURL" => "https://www.facebook.com/a2wroclaw",
            "ShortDesc" => "Centrum Koncertowe A2 we Wroclawiu.",
            "Logo" => $a2Logo,
            "AboutUsContent" => "Centrum Koncertowe A2 przestrzenne i klimatyczne miejsce we Wroclawiu, przystosowane do imprez kulturalnych i eventow; specjalnie wygluszone i wyposazone w sprzet naglosnieniowy i oswietleniowy najwyzszej jakosci.

            Duza przestrzen sprawi, ze kazdy z Was znajdzie sporo miejsca dla siebie i nie bedzie musial stac na „jednej nodze”. Bogato wyposazone bary zaspokoja najbardziej wybrednych milosnikow trunkow, a spora ilosc lozy i stolikow pozwoli przycupnac po koncertowym szalenstwie.
            ",
            "AboutUsPhoto1" => $a2About1,
            "AboutUsPhoto2" => $a2About2,
            "AboutUsPhoto1Alt" => "alt",
            "AboutUsPhoto2Alt" => "alt",
            "OverallRating" => null,
            "OfferRating" => null,
            "PricesRating" => null,
            "SoundRating" => null,
            "AtmRating" => null
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 1,
            "GenreID" => 5,
            "IsPrimary" => true
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 1,
            "GenreID" => 6,
            "IsPrimary" => false
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 1,
            "GenreID" => 4,
            "IsPrimary" => false
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 3,
            "GenreID" => 5,
            "IsPrimary" => true
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 2,
            "GenreID" => 6,
            "IsPrimary" => true
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 2,
            "GenreID" => 4,
            "IsPrimary" => false
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 4,
            "GenreID" => 5,
            "IsPrimary" => true
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 4,
            "GenreID" => 6,
            "IsPrimary" => false
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 4,
            "GenreID" => 4,
            "IsPrimary" => false
        ]);

        DB::table('Club')->insert([
            "SiteStatsID" => null,
            "ClubTypeID" => 1,
            "CityID" => 2,
            "AppUserID" => 1,
            "Name" => "Faust",
            "Address" => "ul. Zlota 1",
            "FbContentURL" => "https://www.facebook.com/KlubFaust",
            "ShortDesc" => "Uslyszysz u nas muzyke rockowa w najszerszym tego slowa znaczeniu - od bluesa po black metal. W weekendy (i nie tylko!) odbywaja sie koncerty przeroznych kapel.",
            "Logo" => $faustLogo,
            "AboutUsContent" => "About",
            "AboutUsPhoto1" => $imagePlaceholder,
            "AboutUsPhoto2" => $imagePlaceholder,
            "AboutUsPhoto1Alt" => "alt",
            "AboutUsPhoto2Alt" => "alt",
            "OverallRating" => null,
            "OfferRating" => null,
            "PricesRating" => null,
            "SoundRating" => null,
            "AtmRating" => null
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 5,
            "GenreID" => 5,
            "IsPrimary" => true
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 5,
            "GenreID" => 6,
            "IsPrimary" => false
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 5,
            "GenreID" => 4,
            "IsPrimary" => false
        ]);

        DB::table('Club')->insert([
            "SiteStatsID" => null,
            "ClubTypeID" => 3,
            "CityID" => 4,
            "AppUserID" => 1,
            "Name" => "B90",
            "Address" => "ul. Elektrykow",
            "FbContentURL" => "https://www.facebook.com/B90klub",
            "ShortDesc" => "B90 jest podmiotem wykonawczym misji portalu muzycznego Soundrive.pl dazacego do zbudowania silnego, alternatywnego srodowiska muzycznego w opozycji do komercyjnej rozrywki.",
            "Logo" => $b90Logo,
            "AboutUsContent" => "About",
            "AboutUsPhoto1" => $imagePlaceholder,
            "AboutUsPhoto2" => $imagePlaceholder,
            "AboutUsPhoto1Alt" => "alt",
            "AboutUsPhoto2Alt" => "alt",
            "OverallRating" => null,
            "OfferRating" => null,
            "PricesRating" => null,
            "SoundRating" => null,
            "AtmRating" => null
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 6,
            "GenreID" => 5,
            "IsPrimary" => true
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 6,
            "GenreID" => 6,
            "IsPrimary" => false
        ]);

        DB::table('ClubGenres')->insert([
            "ClubID" => 6,
            "GenreID" => 4,
            "IsPrimary" => false
        ]);
    }
}
