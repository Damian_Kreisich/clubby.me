<?php

use Illuminate\Database\Seeder;

class ClubTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ClubType')->insert([
            'Name' => "Club"
        ]);
        DB::table('ClubType')->insert([
            'Name' => "Pub"
        ]);
        DB::table('ClubType')->insert([
            'Name' => "Concert Hall"
        ]);
    }
}
