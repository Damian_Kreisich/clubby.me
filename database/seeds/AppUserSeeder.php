<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class AppUserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        require "SeedImage.php";
        DB::table('users')->insert([
            "email" => "masterkrejzi@gmail.com",
            "password" => bcrypt("user1")
        ]);
        DB::table('AppUser')->insert([
            "ID" => 1,
            "CityID" => 1,
            "DisplayName" => "Damian Kreisich",
            "PersonalDesc" => "Some description...",
            "DateOfBirth" => Carbon::parse("2010/10/15")->format('y/m/d'),
            "Photo" => $imagePlaceholder,
            "ContactMail" => "masterkrejzi@gmail.com",
            "Discriminator" => "user"
        ]);
        DB::table('users')->insert([
            "email" => "user2@example.com",
            "password" => bcrypt("user2")
        ]);
        DB::table('AppUser')->insert([
            "ID" => 2,
            "CityID" => 2,
            "DisplayName" => "Jan Kowalski",
            "PersonalDesc" => "Some description...",
            "DateOfBirth" => Carbon::parse("2000/10/15")->format('y/m/d'),
            "Photo" => $imagePlaceholder,
            "ContactMail" => "user2@example.com",
            "Discriminator" => "user"
        ]);
        DB::table('users')->insert([
            "email" => "user3@example.com",
            "password" => bcrypt("user3")
        ]);
        DB::table('AppUser')->insert([
            "ID" => 3,
            "CityID" => 3,
            "DisplayName" => "Aneta Kowalska",
            "PersonalDesc" => "Some description...",
            "DateOfBirth" => Carbon::parse("1997/10/15")->format('y/m/d'),
            "Photo" => $imagePlaceholder,
            "ContactMail" => "user3@example.com",
            "Discriminator" => "user"
        ]);
    }
}
