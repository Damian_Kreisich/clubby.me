<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('City')->insert([
            "Name" => "Wroclaw"
        ]);

        DB::table('City')->insert([
            "Name" => "Katowice"
        ]);

        DB::table('City')->insert([
            "Name" => "Warszawa"
        ]);

        DB::table('City')->insert([
            "Name" => "Gdansk"
        ]);
    }
}
