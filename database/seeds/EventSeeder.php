<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class EventSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        require "SeedImage.php";

        DB::table('Event')->insert([
            "SiteStatsID" => null,
            "ClubID" => 1,
            "Name" => "WØJNA, Zatrata, How Long [Cz], Zozoo [Sk]",
            "Date" => Carbon::parse("2019/11/15")->format('y/m/d'),
            "FbContentURL" => "https://www.facebook.com/events/2869623179843308",
            "ShortDesc" => "Nice black metal event to join and thrash.",
            "TimeStart" => Carbon::parse("10:20")->format('H:i'),
            "TimeEnd" => Carbon::parse("20:10")->format('H:i'),
            "LongDescription" => "Nice black metal event to join and thrash. Longer",
            "SoldOut" => 0,
            "Logo" => $imagePlaceholder
        ]);

        DB::table('Event')->insert([
            "SiteStatsID" => null,
            "ClubID" => 1,
            "Name" => "I Declare War [USA] / Cytotoxin / Acranius / Necrotted",
            "Date" => Carbon::parse("2019/10/29")->format('y/m/d'),
            "FbContentURL" => "https://www.facebook.com/events/621208781693958",
            "ShortDesc" => "Nice heavy metal event to join and thrash.",
            "TimeStart" => Carbon::parse("10:20")->format('H:i'),
            "TimeEnd" => Carbon::parse("20:10")->format('H:i'),
            "LongDescription" => "Nice heavy metal event to join and thrash. Longer",
            "SoldOut" => 1,
            "Logo" => $imagePlaceholder
        ]);


        DB::table('Event')->insert([
            "SiteStatsID" => null,
            "ClubID" => 1,
            "Name" => "Drown My Day/ Tester Gier/ HeadUp",
            "Date" => Carbon::parse("2020/01/10")->format('y/m/d'),
            "FbContentURL" => "https://www.facebook.com/events/777827742649581",
            "ShortDesc" => "Nice crossover event to join and thrash.",
            "TimeStart" => Carbon::parse("10:20")->format('H:i'),
            "TimeEnd" => Carbon::parse("20:10")->format('H:i'),
            "LongDescription" => "Nice crossover event to join and thrash. Longer",
            "SoldOut" => 0,
            "Logo" => $imagePlaceholder
        ]);



        DB::table('Organization')->insert([
            "AppUserID" => 1,
            "EventID" => 1,
            "IsSubOrganizer" => 0
        ]);

        DB::table('Organization')->insert([
            "AppUserID" => 2,
            "EventID" => 1,
            "IsSubOrganizer" => 1
        ]);
        DB::table('Organization')->insert([
            "AppUserID" => 2,
            "EventID" => 2,
            "IsSubOrganizer" => 0
        ]);
        DB::table('Organization')->insert([
            "AppUserID" => 3,
            "EventID" => 3,
            "IsSubOrganizer" => 0
        ]);
        DB::table('EventGenres')->insert([
            "EventID" => 1,
            "GenreID" => 5
        ]);
        DB::table('EventGenres')->insert([
            "EventID" => 1,
            "GenreID" => 6
        ]);
        DB::table('EventGenres')->insert([
            "EventID" => 2,
            "GenreID" => 6
        ]);
        DB::table('EventGenres')->insert([
            "EventID" => 3,
            "GenreID" => 5
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 1,
            "EventIndex" => 1,
            "Bands" => "Wojna"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 1,
            "EventIndex" => 2,
            "Bands" => "Zatrata"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 1,
            "EventIndex" => 3,
            "Bands" => "How Long"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 1,
            "EventIndex" => 4,
            "Bands" => "Zozoo"
        ]);

        DB::table('Event_bands')->insert([
            "EventID" => 2,
            "EventIndex" => 1,
            "Bands" => "I Declare War"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 2,
            "EventIndex" => 2,
            "Bands" => "Cytotoxin"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 2,
            "EventIndex" => 3,
            "Bands" => "Acranius"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 2,
            "EventIndex" => 4,
            "Bands" => "Necrotted"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 3,
            "EventIndex" => 1,
            "Bands" => "Drown My Day"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 3,
            "EventIndex" => 2,
            "Bands" => "Tester Gier"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 3,
            "EventIndex" => 3,
            "Bands" => "HeadUp"
        ]);
        DB::table('Event_ticketSites')->insert([
            "EventID" => 1,
            "EventIndex" => 1,
            "TicketSites" => "https://www.ebilet.pl"
        ]);
        DB::table('Event_ticketSites')->insert([
            "EventID" => 1,
            "EventIndex" => 2,
            "TicketSites" => "https://www.eventim.pl"
        ]);
        DB::table('Event_ticketSites')->insert([
            "EventID" => 2,
            "EventIndex" => 1,
            "TicketSites" => "https://www.ebilet.pl"
        ]);
        DB::table('Event_ticketSites')->insert([
            "EventID" => 3,
            "EventIndex" => 1,
            "TicketSites" => "https://www.eventim.pl"
        ]);

        DB::table('Event')->insert([
            "SiteStatsID" => null,
            "ClubID" => 4,
            "Name" => "Łydka Grubasa / Lej Mi Pół / Mescalero",
            "Date" => Carbon::parse("2020/01/06")->format('y/m/d'),
            "FbContentURL" => "https://www.facebook.com/events/2662597790426189",
            "ShortDesc" => "Zapraszamy na jesienną trasę, która będzie przebiegać pod hasłem Urocza Trasa Łydki Grubasa! Pociśniemy ponad dwadzieścia koncertów od Szczecina po Sanok. Będzie się działo, przybywajcie!",
            "TimeStart" => Carbon::parse("19:00")->format('H:i'),
            "TimeEnd" => Carbon::parse("23:45")->format('H:i'),
            "LongDescription" => "Kochani, znowu jedziemy do Was!

            Zapraszamy na jesienną trasę, która będzie przebiegać pod hasłem Urocza Trasa Łydki Grubasa! Pociśniemy ponad dwadzieścia koncertów od Szczecina po Sanok. Będzie się działo, przybywajcie! Szykujemy dla Was zestaw kawałków znanych i lubianych, ale pogramy też rzeczy, których jeszcze nie było!

            Ta trasa to podziękowanie dla Was i podsumowanie naszej dotychczasowej działalności, okraszone wieloma muzycznymi wygłupami. Niedługo wbijamy do studia i nagrywamy dla Was całkiem nowe kawałki. Ale – póki co – zapraszamy na koncerty. Wykonajmy razem Ścianę Miłości, Krasnoludki, Wycieraczki, Wirujące Lupy – a może to właśnie na WASZYM koncercie wpadnie nam razem do głowy coś równie porąbanego, co na stałe wejdzie do koncertowych harców...?",
            "SoldOut" => 0,
            "Logo" => $lydkaLogo,
            "Attendees" => 300
        ]);
        DB::table('EventGenres')->insert([
            "EventID" => 4,
            "GenreID" => 4
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 4,
            "EventIndex" => 1,
            "Bands" => "Łydka Grubasa"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 4,
            "EventIndex" => 2,
            "Bands" => "Lej Mi Pół"
        ]);
        DB::table('Event_bands')->insert([
            "EventID" => 4,
            "EventIndex" => 3,
            "Bands" => "Mescalero"
        ]);
        DB::table('Event_ticketSites')->insert([
            "EventID" => 4,
            "EventIndex" => 1,
            "TicketSites" => "https://goout.net/pl/bilety/lydka-grubasa+lej-mi-pol+inni/kuye/?fbclid=IwAR0e9RX4Clim3O4ySs8HfgD16S7n59Zov2zOAntJP5GReX7o90v8XPTdHww"
        ]);

        DB::table('Event')->insert([
            "SiteStatsID" => null,
            "ClubID" => 4,
            "Name" => "Bednarek",
            "Date" => Carbon::parse("2020/01/07")->format('y/m/d'),
            "FbContentURL" => "https://www.facebook.com/events/1658030160963914",
            "ShortDesc" => "Bednarek zapowiada trasę koncertową back to the past. Już 17 listopada w Poznaniu formacja Kamila Bednarka rozpocznie cykl energetycznych koncertów, w trakcie których fani będą mogli usłyszeć korzenne reggae, a także utwory z najnowszego albumu.",
            "TimeStart" => Carbon::parse("18:30")->format('H:i'),
            "TimeEnd" => Carbon::parse("21:30")->format('H:i'),
            "LongDescription" => "Bednarek zapowiada trasę koncertową back to the past. Już 17 listopada w Poznaniu formacja Kamila Bednarka rozpocznie cykl energetycznych koncertów, w trakcie których fani będą mogli usłyszeć korzenne reggae, a także utwory z najnowszego albumu. Oprócz Poznania, zespół wystąpi także w Szczecinie, Gdańsku, Wrocławiu, Łodzi, Warszawie oraz Krakowie. Bilety na wydarzenia dostępne w sprzedaży!",
            "SoldOut" => 1,
            "Logo" => $bednarekLogo,
            "Attendees" => 2400
        ]);

        DB::table('EventGenres')->insert([
            "EventID" => 5,
            "GenreID" => 3
        ]);

        DB::table('Event_bands')->insert([
            "EventID" => 5,
            "EventIndex" => 1,
            "Bands" => "Bednarek"
        ]);

        DB::table('Event')->insert([
            "SiteStatsID" => null,
            "ClubID" => 4,
            "Name" => "Łąki Łan",
            "Date" => Carbon::parse("2020/01/13")->format('y/m/d'),
            "FbContentURL" => "https://www.facebook.com/events/1658030160963914",
            "ShortDesc" => "KONCERT PROMUJĄCY NOWĄ PŁYTĘ!
            Wrocław! Czas na szał. Wrocławski cwał. Znamy się dobrze a poznamy jeszcze lepiej.
            Nowy Łan. Nowa płyta. Ruszamy pod Ślężę z kopyta.",
            "TimeStart" => Carbon::parse("19:00")->format('H:i'),
            "TimeEnd" => Carbon::parse("23:00")->format('H:i'),
            "LongDescription" => "Długi opis",
            "SoldOut" => 0,
            "Logo" => $lanLogo,
            "Attendees" => 550
        ]);

        DB::table('EventGenres')->insert([
            "EventID" => 6,
            "GenreID" => 4
        ]);

        DB::table('Event_bands')->insert([
            "EventID" => 6,
            "EventIndex" => 1,
            "Bands" => "Laki Lan"
        ]);

        DB::table('Event_ticketSites')->insert([
            "EventID" => 6,
            "EventIndex" => 1,
            "TicketSites" => "https://wrockfest.pl/bilety-online/?fbclid=IwAR2XzDjzqXcwoQX5d6WWAP6e65KIh9xNXEEQ7CiuvbBw59-X_eT0KqNLVrI"
        ]);
    }
}
