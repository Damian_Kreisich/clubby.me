<?php

use Illuminate\Database\Seeder;

class MusicGenresSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Genre')->insert([
            'Name' => "Country"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Pop"
        ]);
        DB::table('Genre')->insert([
            'Name' => "R'n'b"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Rock"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Heavy Metal"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Blues"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Jazz"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Classical"
        ]);
        DB::table('Genre')->insert([
            'Name' => "Hip-hop"
        ]);
    }
}
