<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EnumSeeder::class);
        $this->call(AppUserSeeder::class);
        $this->call(ClubSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(ReviewSeeder::class);
    }
}
