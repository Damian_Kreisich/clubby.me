#!/bin/bash
sudo rm -rf ~/dbdata
mkdir ~/dbdata
docker network create clubby-net
docker pull mariadb:10
docker pull phpmyadmin/phpmyadmin
docker run --network clubby-net --name mariadb -p 3306:3306 -v ~/dbdata:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=pwd mariadb:10 > /dev/null &
sleep 10
MARIADB_IP=$(docker inspect clubby-net | grep IPv4 | cut -d "\"" -f 4 | cut -d "/" -f 1)
docker run --network clubby-net --name admin -d -e PMA_HOST="$MARIADB_IP" -e PMA_PORT=3306 -p 8080:80 phpmyadmin/phpmyadmin &
cp .env.example .env
echo "------------------------------------------------- TAKE THIS IP ---------"
echo $MARIADB_IP
echo "--------------------------------------------------TAKE THIS IP ---------"
