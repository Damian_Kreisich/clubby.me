<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $ID
 * @property int $EventID
 * @property int $ClubID
 * @property int $AppUserID
 * @property string $Content
 * @property int $Factor1
 * @property int $Factor2
 * @property int $Factor3
 * @property int $Factor4
 * @property int $Overall
 * @property string $Discriminator
 * @property AppUser $appUser
 * @property Event $event
 * @property Club $club
 */
class Review extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Review';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['EventID', 'ClubID', 'AppUserID', 'Content', 'Factor1', 'Factor2', 'Factor3', 'Factor4', 'Overall', 'Discriminator'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appUser()
    {
        return $this->belongsTo('App\AppUser', 'AppUserID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event', 'EventID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('App\Club', 'ClubID', 'ID');
    }
}
