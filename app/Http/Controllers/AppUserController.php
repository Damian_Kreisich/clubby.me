<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\AppUserResource;
use App\Http\Resources\ClubCollectionResource;
use App\Http\Resources\EventCollectionResource;
use App\Http\Resources\ReviewCollectionResource;
use App\AppUser;
use App\City;
use Illuminate\Http\Request;

class AppUserController extends Controller
{
    public function showUserClubs(AppUser $appUser)
    {
        return new ClubCollectionResource($appUser->clubs()->with('city', 'genres')->get());
    }

    public function showOrganizedUserEvents(AppUser $appUser)
    {
        return new EventCollectionResource($appUser->eventsOrganized()->with('club', 'genres')->get());
    }

    public function show(AppUser $appUser): AppUserResource
    {
        return new AppUserResource($appUser);
    }

    public function update(AppUser $appUser, Request $request)
    {
        $request->validate([
            "displayName" => "required|string|unique:AppUser"
        ]);

        // if ($request->user()->ID == $appUser->ID) {
            if (!is_null($request->city)) {
                $cityID = City::firstOrCreate([
                    'Name' => $request->city
                ])->ID;
            } else {
                $cityID = null;
            }

            $appUser->update([
                "CityID" => $cityID,
                "DisplayName" => $request->displayName,
                "PersonalDesc" => $request->description,
                "DateOfBirth" => date("Y-m-d", strtotime($request->dateOfBirth))
            ]);
            return response()->json(new AppUserResource(AppUser::with("city")->find($appUser->ID)), 200);
        // } else {
            // return response()->json(["Message" => "Unauthorized"], 401);
        // }
    }
    public function getReviewsForClub($id){
        return new ReviewCollectionResource(AppUser::find($id)->reviews()->whereNotNull("ClubID")->with('club')->get());
    }
    public function getReviewsForEvent($id){
        return new ReviewCollectionResource(AppUser::find($id)->reviews()->whereNotNull("EventID")->with('event')->get());
    }
}
