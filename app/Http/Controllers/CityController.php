<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityCollectionResource;
use App\Http\Resources\CityResource;
use Illuminate\Http\Request;
use App\City;
use App\Club;
use App\Event;

use App\Http\Resources\ClubCollectionResource;
use App\Http\Resources\EventCollectionResource;
use Carbon\Carbon;

class CityController extends Controller
{
    function index() : CityCollectionResource{
        return new CityCollectionResource(City::all(['Name']));
    }
    function indexClubs(Request $request, City $city){
        $query = $city->clubs()->with('city', 'genres')->get();
        if (!is_null($request->query('limit'))) {
            if($request->query('limit') < $query->count())
                $query = $query->random($request->query('limit'));
        }
        return new ClubCollectionResource($query);
    }
    function indexEvents(Request $request, City $city){
        $query = $city->events()->map(function ($item, $key) {
            return $item->with('club', 'genres')->where("Date", ">=", Carbon::now())->get();;
        });
        $query = $query->flatten();

        if (!is_null($request->query('limit'))) {
            if($request->query('limit') < $query->count())
                $query = $query->random($request->query('limit'));
        }
        return new EventCollectionResource($query);
    }
    public function show(City $city): CityResource{
        return new CityResource($city);
    }
}
