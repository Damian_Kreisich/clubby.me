<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Http\Resources\GenresCollectionResource;
use Illuminate\Http\Request;

class GenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() : GenresCollectionResource
    {
        return new GenresCollectionResource(Genre::all());
    }
}
