<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Event;
use App\Club;
use App\Genre;
use App\EventGenres;
use App\Event_bands;
use App\Event_ticketSites;
use App\Http\Resources\EventCollectionResource;
use App\Http\Resources\EventResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\ReviewCollectionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Event::with('club', 'genres')->where("Date", ">=", Carbon::now())->get();
        if (!is_null($request->query('column'))){
            $query = Event::with('club', 'genres')->where("Date", ">=", Carbon::now())->get($request->query('column'));
        }
        if (!is_null($request->query('limit'))) {
            if($request->query('limit') < $query->count())
                $query = $query->random($request->query('limit'));
        }

        return new EventCollectionResource($query);
    }

    public function search(Request $request)
    {
        $query = Event::with("club", "genres")->where("Name", "like", "%".$request->input("searchTerm")."%");
        $query = $query->when($request->input("city") != "", function($q) use ($request){
            return $q -> whereHas("club", function($q) use ($request){
                return $q->whereHas("city", function($q) use ($request){
                    return $q->where("Name", "like", "%".$request->input("city")."%");
                });
            });
        });
        $query = $query->when(!empty($request->input("placeTypesArray")), function($q) use ($request){
            return $q -> whereHas("club", function ($q) use ($request){
                return $q -> whereIn("ClubTypeID", $request->input("placeTypesArray"));
            });
        });
        $query = $query->when(!empty($request->input("genresArray")), function($q) use ($request){
            return $q -> whereHas('genres', function ($q) use ($request){
                return $q -> whereIn('Name', $request->input("genresArray"));
            });
        });
        $query = $query->when($request->input("dateFrom"), function($q) use ($request){
            return $q->where("Date", ">=", $request->input("dateFrom"));
        });
        $query = $query->when($request->input("dateTo"), function($q) use ($request){
            return $q->where("Date", "<=", $request->input("dateTo"));
        });

        return new EventCollectionResource($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "description" => "nullable|string",
            "timeEnd" => "required|date_format:H:i:s",
            "timeStart" => "required|date_format:H:i:s",
            "name" => "required|string|unique:Event",
            "date" => "required|date",
            "logo" => "nullable|string",
            "fbContentURL" => "nullable|URL",
            "soldOut" => "boolean",
            "longDescription" => "nullable|string",
            "club" => "required|string|exists:Club,Name",
            "bands" => "array",
            "genres" => "array",
            "ticketSites" => "array"
        ]);
        $event = null;
        $clubID = Club::where("Name", $request->input("club"))->first()->ID;
        $event = DB::transaction(function () use ($clubID, $request){
            $event = Event::create([
                "ClubID" => $clubID,
                "SiteStatsID" => null,
                "Name" => $request->input("name"),
                "Date" => $request->input("date"),
                "ShortDesc" => $request->input("description"),
                "FbContentURL" => $request->input("fbContentURL"),
                "TimeStart" => $request->input("timeStart"),
                "TimeEnd" => $request->input("timeEnd"),
                "LongDescription" => $request->input("longDescription"),
                "SoldOut" => $request->input("soldOut"),
                "Logo" => $request->input("logo"),
            ]);

            for ($i = 0; $i < count($request->input("genres")); $i++){
                EventGenres::firstOrCreate([
                    "EventID" => $event->ID,
                    "GenreID" => Genre::where("Name", "=", $request->input("genres")[$i])->first()->ID
                ]);
            }
            $bands = [];
            for ($i = 0; $i < count($request->input("bands")); $i++){
                array_push($bands,[
                    "Bands" => $request->input("bands")[$i],
                    "EventIndex" => $i
                ]);
            }
            $event->eventBands()->createMany($bands);
            $ticket = [];
            for ($i = 0; $i < count($request->input("ticketSites")); $i++){
                array_push($ticket,[
                    "TicketSites" => $request->input("ticketSites")[$i],
                    "EventIndex" => $i
                ]);
            }
            $event->eventTicketSites()->createMany($ticket);
            $event->organizations()->create([
                "AppUserID" => $request->user()->ID
            ]);

            return $event;
        });

        return new JsonResponse([
            "ID" => $event->ID
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new EventResource(Event::with('club', 'genres', "eventBands", "eventTicketSites")->find($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clubID = Club::where("Name", $request->input("club"))->first()->ID;
        $event = Event::find($id);
        $event -> update([
            "ClubID" => $clubID,
            "SiteStatsID" => null,
            "Name" => $request->input("name"),
            "Date" => $request->input("date"),
            "ShortDesc" => $request->input("description"),
            "FbContentURL" => $request->input("fbContentURL"),
            "TimeStart" => $request->input("timeStart"),
            "TimeEnd" => $request->input("timeEnd"),
            "LongDescription" => $request->input("longDescription"),
            "SoldOut" => $request->input("soldOut"),
            "Logo" => $request->input("logo"),
        ]);

        EventGenres::where("EventID", $event->ID)->delete();
        for ($i = 0; $i < count($request->input("genres")); $i++){
            EventGenres::firstOrCreate([
                "EventID" => $event->ID,
                "GenreID" => Genre::where("Name", "=", $request->input("genres")[$i])->first()->ID
            ]);
        }
        $event->eventBands()->delete();
        $bands = [];
        for ($i = 0; $i < count($request->input("bands")); $i++){
            array_push($bands,[
                "Bands" => $request->input("bands")[$i],
                "EventIndex" => $i
            ]);
        }
        $event->eventBands()->createMany($bands);
        $event->eventTicketSites()->delete();
        $ticket = [];
        for ($i = 0; $i < count($request->input("ticketSites")); $i++){
            array_push($ticket,[
                "TicketSites" => $request->input("ticketSites")[$i],
                "EventIndex" => $i
            ]);
        }
        $event->eventTicketSites()->createMany($ticket);

        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::find($id)->reviews()->delete();
        return (Event::destroy($id));
    }

    public function indexReviews(Request $request, $id){
        $query = Event::find($id) -> reviews() -> with('appUser') -> get();
        return new ReviewCollectionResource($query);
    }

    public function checkAttendance(Request $request){
        $attendance = Attendance::where("AppUserID", "=", $request->query("user"))->where("EventID", "=", $request->query("event"))->first();
        return is_null($attendance)? new Response("Unattended", 204) : new Response("Attended", 200);
    }
    public function addAttendance(Request $request){
        return Attendance::create([
            "AppUserID" => $request->input("user"),
            "EventID" => $request->input("event")
        ]);
    }
    public function deleteAttendance(Request $request){
        return Attendance::where("AppUserID", "=", $request->input("user"))->where("EventID", "=", $request->input("event"))->first()->delete();
    }
}
