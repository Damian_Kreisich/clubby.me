<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ReviewResource;
use App\Review;
use App\Utility;

class ReviewController extends Controller
{
    public function show($id)
    {
        return new ReviewResource(Review::with("appUser", "club", "club.genres", "club.city", "event", "event.club", "event.genres")->find($id));
    }

    public function checkClubUser(Request $request)
    {
        return new ReviewResource(Review::where("AppUserID", "=", $request->query("user"))->where("ClubID", "=", $request->query("club"))->get());
    }

    public function checkEventUser(Request $request)
    {
        return new ReviewResource(Review::where("AppUserID", "=", $request->query("user"))->where("EventID", "=", $request->query("event"))->get());
    }
    public function destroy($id, Request $request)
    {
        if ($request->user()->ID == Review::find($id)->AppUserID) {
            return (Review::destroy($id));
        } else {
            return response()->json(["Message" => "Unauthorized"], 401);
        }
    }
    public function store(Request $request)
    {
        Review::create([
            'ClubID' => $request->input("club"),
            "EventID" => $request->input("event"),
            "AppUserID" => $request->input("user"),
            "Content" => $request->input("message"),
            "Factor1" => $request->input("factor1"),
            "Factor2" => $request->input("factor2"),
            "Factor3" => $request->input("factor3"),
            "Factor4" => $request->input("factor4"),
            "Overall" => Utility::countAvg($request->input("factor1"), $request->input("factor2"), $request->input("factor3"), $request->input("factor4")),
            "Discriminator" => $request->input("type")
        ]);
    }
    public function update(Request $request, $id)
    {
        $review = Review::find($id);
        if ($request->user()->ID == $review->AppUserID) {
            $review->update([
                "Content" => $request->input("message"),
                "Factor1" => $request->input("factor1"),
                "Factor2" => $request->input("factor2"),
                "Factor3" => $request->input("factor3"),
                "Factor4" => $request->input("factor4"),
                "Overall" => Utility::countAvg($request->input("factor1"), $request->input("factor2"), $request->input("factor3"), $request->input("factor4"))
            ]);
        } else {
            return response()->json(["Message" => "Unauthorized"], 401);
        }
    }
}
