<?php

namespace App\Http\Controllers;

use App\Club;
use App\City;
use App\ClubGenres;
use App\ClubType;
use App\Genre;
use App\Http\Resources\ClubCollectionResource;
use App\Http\Resources\ClubResource;
use App\Http\Resources\EventCollectionResource;
use App\Http\Resources\ReviewCollectionResource;
use Illuminate\Http\Request;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use Zend\Diactoros\Response\JsonResponse;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Club::with('city', 'genres')->get();
        if (!is_null($request->query('column'))){
            $query = Club::all($request->query('column'));
        }
        if (!is_null($request->query('limit'))) {
            if($request->query('limit') < $query->count())
                $query = $query->random($request->query('limit'));
        }

        return new ClubCollectionResource($query);
    }

    public function indexEvents(Request $request, $id){
        $query = Club::find($id) -> events()->with('club', 'genres')->where("Date", ">=", Carbon::now())->orderBy("Date", "asc")->get();
        return new EventCollectionResource($query);
    }

    public function indexReviews(Request $request, $id){
        $query = Club::find($id) -> reviews() -> with('appUser') -> get();
        return new ReviewCollectionResource($query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'string',
            'city' => 'required|string',
            'logo' => 'nullable|string',
            'name' => 'required|string|unique:Club',
            'clubType' => 'nullable|exists:ClubType,Name',
            'fbContentURL' => 'nullable|URL',
            'address' => 'string',
            'aboutPhoto1' => 'nullable|string',
            'aboutPhoto2' => 'nullable|string',
            'aboutContent' => 'nullable|string',
            'genres' => 'nullable|array'
        ]);
        if(!is_null($request->city)){
            $cityID = City::firstOrCreate([
                'Name' => $request->city
            ])->ID;
        }
        $clubID = Club::create([
            'SiteStatsID' => null,
            'ClubTypeID' => ClubType::where("Name", "=", $request->input("clubType"))->first()->ID,
            'CityID' => $cityID,
            'AppUserID' => $request->user()->ID,
            'Name' => $request->input("name"),
            'Address' => $request->input("address"),
            "ShortDesc" => $request->input("description"),
            "FbContentURL" => $request->input("fbContentURL"),
            'Logo' => $request->input("logo"),
            'AboutUsContent' => $request->input("aboutContent"),
            'AboutUsPhoto1' => $request->input("aboutPhoto1"),
            'AboutUsPhoto2' => $request->input("aboutPhoto2")
        ])->ID;
        for ($i = 0; $i < count($request->input("genres")); $i++){
            ClubGenres::firstOrCreate([
                "ClubID" => $clubID,
                "GenreID" => Genre::where("Name", "=", $request->input("genres")[$i])->first()->ID
            ]);
        }
        return new JsonResponse([
            "ID" => $clubID
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ClubResource(Club::with("genres", "city", "clubType")->find($id));
    }

    public function search(Request $request){
        $query = Club::with("city", "genres")->where("Name", "like", "%".$request->input("searchTerm")."%");
        $query = $query->when($request->input("city") != "", function($q) use ($request){
            return $q -> whereHas("city", function($q) use ($request){
                return $q->where("Name", "like", "%".$request->input("city")."%");
            });
        });

        $query = $query->when(!empty($request->input("genresArray")), function($q) use ($request){
            return $q -> whereHas('genres', function ($q) use ($request){
                return $q -> whereIn('Name', $request->input("genresArray"));
            });
        });
        $query = $query->when(!empty($request->input("placeTypesArray")), function($q) use ($request){
            return $q -> whereIn("ClubTypeID", $request->input("placeTypesArray"));
        });

        $ratings = $request->input("ratings");
        $query = $query->when($ratings['overall'] > 0, function($q) use ($ratings){
            return $q -> where("OverallRating", ">=", $ratings['overall']);
        });
        $query = $query->when($ratings['sound'] > 0, function($q) use ($ratings){
            return $q -> where("SoundRating", ">=", $ratings['sound']);
        });
        $query = $query->when($ratings['atmosphere'] > 0, function($q) use ($ratings){
            return $q -> where("AtmRating", ">=", $ratings['atmosphere']);
        });
        $query = $query->when($ratings['offer'] > 0, function($q) use ($ratings){
            return $q -> where("OfferRating", ">=", $ratings['offer']);
        });
        $query = $query->when($ratings['prices'] > 0, function($q) use ($ratings){
            return $q -> where("PricesRating", ">=", $ratings['prices']);
        });

        return new ClubCollectionResource($query->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!is_null($request->city)){
            $cityID = City::firstOrCreate([
                'Name' => $request->city
            ])->ID;
        }
        $club = Club::find($id);
        $updateArray = [
            'SiteStatsID' => null,
            'ClubTypeID' => ClubType::where("Name", "=", $request->input("clubType"))->first()->ID,
            'CityID' => $cityID,
            'AppUserID' => $request->user()->ID,
            'Address' => $request->input("address"),
            "ShortDesc" => $request->input("description"),
            "FbContentURL" => $request->input("fbContentURL"),
            'Logo' => $request->input("logo"),
            'AboutUsContent' => $request->input("aboutContent"),
            'AboutUsPhoto1' => $request->input("aboutPhoto1"),
            'AboutUsPhoto2' => $request->input("aboutPhoto2")
        ];
        if ($request -> input("name") != $club->Name){
            $updateArray['Name'] = $request -> input("name");
        }
        Club::find($id)->update($updateArray);
        ClubGenres::where("ClubID", "=", $id)->delete();
        for ($i = 0; $i < count($request->input("genres")); $i++){
            ClubGenres::firstOrCreate([
                "ClubID" => $id,
                "GenreID" => Genre::where("Name", "=", $request->input("genres")[$i])->first()->ID
            ]);
        }

        return new ClubResource(Club::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Club::find($id)->reviews()->delete();
        return (Club::destroy($id));
    }
}
