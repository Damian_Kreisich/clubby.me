<?php

namespace App\Http\Controllers\Contact;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;
use App\Mail\Message;
use App\Club;
use App\Event;

class ContactController extends Controller
{
    public function sendEmail(Request $request) {
    try{
        Mail::to($request->input("email"))
        ->send(new Message($request->input('content'), $request->input('sender'), $request->input('name')));
    }
    catch(Exception $e){
        return ["status" => "failed"];
    }
        return ["status" => "success"];
    }

    public function getAdminMail(){
        return [
            "mail" => env("ADMIN_MAIL", "")
        ];
    }
    public function getClubAdminMail($id){
        $mail = Club::find($id)->appUser()->get();
        $mail = $mail[0]->ContactMail;
        return [
            "mail" => $mail
        ];
    }
    public function getEventAdminMail($id){
        $mail = Event::find($id)->organizers()->get();
        $mail = $mail[0]->ContactMail;
        return [
            "mail" => $mail
        ];
    }
}
