<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $AppUserID
 * @property int $EventID
 * @property boolean $IsSubOrganizer
 * @property Event $event
 * @property AppUser $appUser
 */
class Organization extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Organization';

    /**
     * @var array
     */
    protected $fillable = ["AppUserID", 'IsSubOrganizer'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event', 'EventID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appUser()
    {
        return $this->belongsTo('App\AppUser', 'AppUserID', 'ID');
    }
}
