<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $EventID
 * @property int $EventIndex
 * @property string $TicketSites
 * @property Event $event
 */
class Event_ticketSites extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Event_ticketSites';

    /**
     * @var array
     */
    protected $fillable = ['TicketSites', 'EventIndex'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event', 'EventID', 'ID');
    }
}
