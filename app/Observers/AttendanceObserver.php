<?php

namespace App\Observers;

use App\Event;

class AttendanceObserver
{
    public function created($attendance){
        $event = Event::find($attendance->EventID);
        $event->update([
            "Attendees" => $event->appUsers()->count()
        ]);
    }
    public function deleted($attendance){
        $event = Event::find($attendance->EventID);
        $event->update([
            "Attendees" => $event->appUsers()->count()
        ]);
    }
}
