<?php

namespace App\Observers;

use App\AppUser;
use App\Event;
use App\Club;
use App\Utility;

class ReviewObserver
{
    private function countEventsFactor($reviews)
    {
        $factor1 = round($reviews->avg("Factor1"), 0);
        $factor2 = round($reviews->avg("Factor2"), 0);
        $factor3 = round($reviews->avg("Factor3"), 0);
        $factor4 = round($reviews->avg("Factor4"), 0);
        $overall = Utility::countAvg($factor1, $factor2, $factor3, $factor4);
        return $overall;
    }
    private function countClubsFactor($reviews)
    {
        $factor1 = round($reviews->avg("Factor1"), 0);
        $factor2 = round($reviews->avg("Factor2"), 0);
        $factor3 = round($reviews->avg("Factor3"), 0);
        $factor4 = round($reviews->avg("Factor4"), 0);
        $overall = Utility::countAvg($factor1, $factor2, $factor3, $factor4);
        return [$factor1, $factor2, $factor3, $factor4, $overall];
    }
    private function standardUpdate($review){
        $content = [];
        if (!is_null($review->EventID)) {
            $content = AppUser::find($review->AppUserID);
            $reviews = Event::find($review->EventID)->reviews();

            $overall = $this->countEventsFactor($reviews);
            $content->update([
                "OverallRating" => $overall
            ]);
        } else {
            $content = Club::find($review->ClubID);
            $reviews = $content->reviews();
            $factors = $this->countClubsFactor($reviews);

            $factor1 = $factors[0];
            $factor2 = $factors[1];
            $factor3 = $factors[2];
            $factor4 = $factors[3];
            $overall = $factors[4];

            $content->update([
                "OverallRating" => $overall,
                "SoundRating" => $factor1,
                "AtmRating" => $factor2,
                "OfferRating" => $factor3,
                "PricesRating" => $factor4
            ]);
        }
    }

    public function created($review)
    {
        $this->standardUpdate($review);
    }
    public function deleted($review)
    {
        $this->standardUpdate($review);
    }
    public function updated($review)
    {
        $this->standardUpdate($review);
    }
}
