<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $EventID
 * @property int $GenreID
 * @property Genre $genre
 * @property Event $event
 */
class EventGenres extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'EventGenres';

    /**
     * @var array
     */
    protected $fillable = ["EventID", "GenreID"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genre()
    {
        return $this->belongsTo('App\Genre', 'GenreID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event', 'EventID', 'ID');
    }
}
