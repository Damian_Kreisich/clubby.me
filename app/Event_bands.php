<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $EventID
 * @property int $EventIndex
 * @property string $Bands
 * @property Event $event
 */
class Event_bands extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Event_bands';

    /**
     * @var array
     */
    protected $fillable = ['EventIndex', 'Bands'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event', 'EventID', 'ID');
    }
}
