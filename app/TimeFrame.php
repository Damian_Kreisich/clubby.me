<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ClubID
 * @property int $ID
 * @property string $Name
 * @property string $Open
 * @property string $Close
 * @property Club $club
 */
class TimeFrame extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'TimeFrame';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['ClubID', 'Name', 'Open', 'Close'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('App\Club', 'ClubID', 'ID');
    }
}
