<?php

namespace App;

class Utility {
    public static function countAvg($input1, $input2, $input3, $input4): int {
        return (int)round(($input1 + $input2 + $input3 + $input4)/4, 0);
    }
}
