<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ID
 * @property string $Name
 * @property AppUser[] $appUsers
 * @property Club[] $clubs
 */
class City extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'City';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['Name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appUsers()
    {
        return $this->hasMany('App\AppUser', 'CityID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubs()
    {
        return $this->hasMany('App\Club', 'CityID', 'ID');
    }
    public function events()
    {
        $clubs = $this->clubs()->get();
        $events = $clubs->map(function ($item, $key) {
            return $item->events();
        });

        return $events;
    }
}
