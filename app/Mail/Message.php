<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;



class Message extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $sender;
    public $message;
    public $senderName;

    public function __construct($message, $sender, $senderName)
    {
        $this->sender = $sender;
        $this->message = $message;
        $this->senderName = $senderName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->sender)->view('mail')->with([
            'content' => $this->message,
            'sender' => $this->sender,
            "name" => $this->senderName
        ]);
    }
}
