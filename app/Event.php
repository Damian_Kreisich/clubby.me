<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $ID
 * @property int $ClubID
 * @property int $SiteStatsID
 * @property string $Name
 * @property string $Date
 * @property string $ShortDesc
 * @property string $FbContentURL
 * @property string $TimeStart
 * @property string $TimeEnd
 * @property string $LongDescription
 * @property boolean $SoldOut
 * @property string $Logo
 * @property int $Attendees
 * @property SiteStat $siteStat
 * @property Club $club
 * @property AppUser[] $appUsers
 * @property Genre[] $genres
 * @property EventBand[] $eventBands
 * @property EventTicketSite[] $eventTicketSites
 * @property Organization[] $organizations
 * @property Review[] $reviews
 */
class Event extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Event';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['ClubID', 'SiteStatsID', 'Name', 'Date', 'ShortDesc', 'FbContentURL', 'TimeStart', 'TimeEnd', 'LongDescription', 'SoldOut', 'Logo', 'Attendees'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function siteStat()
    {
        return $this->belongsTo('App\SiteStat', 'SiteStatsID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('App\Club', 'ClubID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function appUsers()
    {
        return $this->belongsToMany('App\AppUser', 'Attendance', 'EventID', 'AppUserID');
    }

    public function organizers()
    {
        return $this->belongsToMany('App\AppUser', 'Organization', 'EventID', 'AppUserID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'EventGenres', 'EventID', 'GenreID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventBands()
    {
        return $this->hasMany('App\Event_bands', 'EventID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventTicketSites()
    {
        return $this->hasMany('App\Event_ticketSites', 'EventID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organizations()
    {
        return $this->hasMany('App\Organization', 'EventID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Review', 'EventID', 'ID');
    }
}
