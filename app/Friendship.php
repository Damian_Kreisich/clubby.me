<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $AppUserID2
 * @property int $AppUserID
 * @property AppUser $appUser
 * @property AppUser $appUser
 */
class Friendship extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Friendship';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appUser()
    {
        return $this->belongsTo('App\AppUser', 'AppUserID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appUser2()
    {
        return $this->belongsTo('App\AppUser', 'AppUserID2', 'ID');
    }
}
