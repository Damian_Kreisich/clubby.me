<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ClubID
 * @property int $GenreID
 * @property boolean $IsPrimary
 * @property Genre $genre
 * @property Club $club
 */
class ClubGenres extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ClubGenres';

    /**
     * @var array
     */
    protected $fillable = ["ClubID","GenreID",'IsPrimary'];
    protected $primaryKey = ["ClubID", "GenreID"];
    public $incrementing = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genre()
    {
        return $this->belongsTo('App\Genre', 'GenreID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('App\Club', 'ClubID', 'ID');
    }
}
