<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ID
 * @property int $CityID
 * @property string $DisplayName
 * @property string $PersonalDesc
 * @property string $DateOfBirth
 * @property string $Photo
 * @property string $ContactMail
 * @property int $OverallRating
 * @property string $Discriminator
 * @property User $user
 * @property City $city
 * @property Event[] $events
 * @property Club[] $clubs
 * @property Genre[] $genres
 * @property AppUser[] $appUsers
 * @property Medium[] $media
 * @property Organization[] $organizations
 * @property Review[] $reviews
 */
class AppUser extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'AppUser';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    protected $with = ['city'];
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['ID', 'CityID', 'DisplayName', 'PersonalDesc', 'DateOfBirth', 'Photo', 'ContactMail', 'OverallRating', 'Discriminator'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'ID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'CityID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany('App\Event', 'Attendance', 'AppUserID', 'EventID');
    }
    public function eventsOrganized()
    {
        return $this->belongsToMany('App\Event', 'Organization', 'AppUserID', 'EventID');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubs()
    {
        return $this->hasMany('App\Club', 'AppUserID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'Favourites', 'AppUserID', 'GenreID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function appUsers()
    {
        return $this->belongsToMany('App\AppUser', 'Friendship', 'AppUserID', 'AppUserID2');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function media()
    {
        return $this->hasMany('App\Medium', 'AppUserID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organizations()
    {
        return $this->hasMany('App\Organization', 'AppUserID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Review', 'AppUserID', 'ID');
    }
}
