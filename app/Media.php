<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $MediaLogosID
 * @property int $AppUserID
 * @property string $MediaUrl
 * @property MediaLogo $mediaLogo
 * @property AppUser $appUser
 */
class Media extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Media';

    /**
     * @var array
     */
    protected $fillable = ['MediaUrl'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mediaLogo()
    {
        return $this->belongsTo('App\MediaLogo', 'MediaLogosID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appUser()
    {
        return $this->belongsTo('App\AppUser', 'AppUserID', 'ID');
    }
}
