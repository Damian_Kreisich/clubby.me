<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ID
 * @property string $Name
 * @property ClubGenre[] $clubGenres
 * @property Event[] $events
 * @property AppUser[] $appUsers
 */
class Genre extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Genre';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['Name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubGenres()
    {
        return $this->hasMany('App\ClubGenre', 'GenreID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany('App\Event', 'EventGenres', 'GenreID', 'EventID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function appUsers()
    {
        return $this->belongsToMany('App\AppUser', 'Favourites', 'GenreID', 'AppUserID');
    }
}
