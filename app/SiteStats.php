<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ID
 * @property int $OverallTimeSpent
 * @property int $AverageTimeSpent
 * @property int $Entries
 * @property int $InteractivityIndex
 * @property Club[] $clubs
 * @property Event[] $events
 */
class SiteStats extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'SiteStats';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['OverallTimeSpent', 'AverageTimeSpent', 'Entries', 'InteractivityIndex'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubs()
    {
        return $this->hasMany('App\Club', 'SiteStatsID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('App\Event', 'SiteStatsID', 'ID');
    }
}
