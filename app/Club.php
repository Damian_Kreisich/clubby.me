<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $ID
 * @property int $SiteStatsID
 * @property int $ClubTypeID
 * @property int $CityID
 * @property int $AppUserID
 * @property string $Name
 * @property string $Address
 * @property string $ShortDesc
 * @property string $FbContentURL
 * @property string $Logo
 * @property string $AboutUsContent
 * @property string $AboutUsPhoto1
 * @property string $AboutUsPhoto2
 * @property string $AboutUsPhoto1Alt
 * @property string $AboutUsPhoto2Alt
 * @property int $OverallRating
 * @property int $OfferRating
 * @property int $PricesRating
 * @property int $SoundRating
 * @property int $AtmRating
 * @property ClubType $clubType
 * @property AppUser $appUser
 * @property SiteStat $siteStat
 * @property City $city
 * @property ClubGenre[] $clubGenres
 * @property Event[] $events
 * @property Review[] $reviews
 * @property TimeFrame[] $timeFrames
 */
class Club extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Club';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * @var array
     */
    protected $fillable = ['SiteStatsID', 'ClubTypeID', 'CityID', 'AppUserID', 'Name', 'Address', 'ShortDesc', 'FbContentURL', 'Logo', 'AboutUsContent', 'AboutUsPhoto1', 'AboutUsPhoto2', 'AboutUsPhoto1Alt', 'AboutUsPhoto2Alt', 'OverallRating', 'OfferRating', 'PricesRating', 'SoundRating', 'AtmRating'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clubType()
    {
        return $this->belongsTo('App\ClubType', 'ClubTypeID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function appUser()
    {
        return $this->belongsTo('App\AppUser', 'AppUserID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function siteStat()
    {
        return $this->belongsTo('App\SiteStat', 'SiteStatsID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'CityID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubGenres()
    {
        return $this->hasMany('App\ClubGenres', 'ClubID', 'ID');
    }
    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'ClubGenres', 'ClubID', 'GenreID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('App\Event', 'ClubID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Review', 'ClubID', 'ID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeFrames()
    {
        return $this->hasMany('App\TimeFrame', 'ClubID', 'ID');
    }

}
