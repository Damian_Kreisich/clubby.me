<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('city', 'CityController@index');
Route::get('city/{city}','CityController@show');
Route::post('contact', 'Contact\\ContactController@sendEmail');
Route::get('contact/admin', 'Contact\\ContactController@getAdminMail');

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource("club", "ClubController");
    Route::apiResource("event", "EventController");
    Route::apiResource("review", "ReviewController");
    Route::post("search/club", "ClubController@search");
    Route::post("search/event", "EventController@search");
    Route::post("attendance", "EventController@addAttendance");
    Route::get("attendance", "EventController@checkAttendance");
    Route::delete("attendance", "EventController@deleteAttendance");
    Route::get("appUser/{appUser}", 'AppUserController@show');
    Route::put("appUser/{appUser}", "AppUserController@update");
    Route::get("appUser/club/{appUser}", "AppUserController@showUserClubs");
    Route::get("appUser/event/{appUser}", "AppUserController@showOrganizedUserEvents");
    Route::get("genre", 'GenresController@index');
    Route::get("club/city/{city}", "CityController@indexClubs");
    Route::get("event/city/{city}", "CityController@indexEvents");
    Route::get("review/club/check", "ReviewController@checkClubUser");
    Route::get("review/event/check", "ReviewController@checkEventUser");
    Route::get("review/user/club/{user}", "AppUserController@getReviewsForClub");
    Route::get("review/user/event/{user}", "AppUserController@getReviewsForEvent");
    Route::get("event/club/{club}", "ClubController@indexEvents");
    Route::get("review/event/{event}", "EventController@indexReviews");
    Route::get("review/club/{club}", "ClubController@indexReviews");
    Route::get("review/{review}", "ReviewController@show");
    Route::get("contact/club/{club}", "Contact\\ContactController@getClubAdminMail");
    Route::get("contact/event/{event}", "Contact\\ContactController@getEventAdminMail");
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});
