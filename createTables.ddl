CREATE TABLE AppUser (
  ID            int(11) NOT NULL, 
  CityID        int(10), 
  DisplayName   varchar(255) NOT NULL UNIQUE, 
  PersonalDesc  varchar(255), 
  DateOfBirth   date, 
  Photo         longtext, 
  ContactMail   varchar(255), 
  OverallRating int(10), 
  Discriminator varchar(255) NOT NULL, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE Attendance (
  EventID   int(10) NOT NULL, 
  AppUserID int(11) NOT NULL, 
  PRIMARY KEY (EventID, 
  AppUserID)) ENGINE=InnoDB;
CREATE TABLE City (
  ID   int(10) NOT NULL AUTO_INCREMENT, 
  Name varchar(255) NOT NULL UNIQUE, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE Club (
  ID               int(10) NOT NULL AUTO_INCREMENT, 
  SiteStatsID      int(10), 
  ClubTypeID       int(10) NOT NULL, 
  CityID           int(10) NOT NULL, 
  AppUserID        int(11) NOT NULL, 
  Name             varchar(255) NOT NULL UNIQUE, 
  Address          varchar(255), 
  ShortDesc        varchar(255), 
  FbContentURL     varchar(255), 
  Logo             longtext, 
  AboutUsContent   longtext, 
  AboutUsPhoto1    longtext, 
  AboutUsPhoto2    longtext, 
  AboutUsPhoto1Alt varchar(255), 
  AboutUsPhoto2Alt varchar(255), 
  OverallRating    int(10), 
  OfferRating      int(10), 
  PricesRating     int(10), 
  SoundRating      int(10), 
  AtmRating        int(10), 
  deleted_at       timestamp NULL, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE ClubGenres (
  ClubID    int(10) NOT NULL, 
  GenreID   int(10) NOT NULL, 
  IsPrimary tinyint(1), 
  PRIMARY KEY (ClubID, 
  GenreID)) ENGINE=InnoDB;
CREATE TABLE ClubType (
  ID   int(10) NOT NULL AUTO_INCREMENT, 
  Name varchar(255) NOT NULL UNIQUE, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE Event (
  ID              int(10) NOT NULL AUTO_INCREMENT, 
  ClubID          int(10) NOT NULL, 
  SiteStatsID     int(10), 
  Name            varchar(255) NOT NULL UNIQUE, 
  `Date`          date NOT NULL UNIQUE, 
  ShortDesc       varchar(255), 
  FbContentURL    varchar(255), 
  TimeStart       time, 
  TimeEnd         time, 
  LongDescription longtext, 
  SoldOut         tinyint(1), 
  Logo            longtext, 
  Attendees       int(10), 
  deleted_at      timestamp NULL, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE Event_bands (
  EventID    int(10) NOT NULL, 
  EventIndex int(11) NOT NULL, 
  Bands      varchar(255), 
  PRIMARY KEY (EventID, 
  EventIndex)) ENGINE=InnoDB;
CREATE TABLE Event_ticketSites (
  EventID     int(10) NOT NULL, 
  EventIndex  int(11) NOT NULL, 
  TicketSites varchar(255), 
  PRIMARY KEY (EventID, 
  EventIndex)) ENGINE=InnoDB;
CREATE TABLE EventGenres (
  EventID int(10) NOT NULL, 
  GenreID int(10) NOT NULL, 
  PRIMARY KEY (EventID, 
  GenreID)) ENGINE=InnoDB;
CREATE TABLE Favourites (
  GenreID   int(10) NOT NULL, 
  AppUserID int(11) NOT NULL, 
  PRIMARY KEY (GenreID, 
  AppUserID)) ENGINE=InnoDB;
CREATE TABLE Friendship (
  AppUserID2 int(11) NOT NULL, 
  AppUserID  int(11) NOT NULL, 
  PRIMARY KEY (AppUserID2, 
  AppUserID)) ENGINE=InnoDB;
CREATE TABLE Genre (
  ID   int(10) NOT NULL AUTO_INCREMENT, 
  Name varchar(255) NOT NULL UNIQUE, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE Media (
  MediaLogosID int(10) NOT NULL, 
  AppUserID    int(11) NOT NULL, 
  MediaUrl     varchar(255), 
  PRIMARY KEY (MediaLogosID, 
  AppUserID)) ENGINE=InnoDB;
CREATE TABLE MediaLogos (
  ID     int(10) NOT NULL AUTO_INCREMENT, 
  `File` longtext NOT NULL UNIQUE, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE Organization (
  AppUserID      int(11) NOT NULL, 
  EventID        int(10) NOT NULL, 
  IsSubOrganizer tinyint(1), 
  PRIMARY KEY (AppUserID, 
  EventID)) ENGINE=InnoDB;
CREATE TABLE Review (
  ID            int(10) NOT NULL AUTO_INCREMENT, 
  EventID       int(10), 
  ClubID        int(10), 
  AppUserID     int(11), 
  Content       longtext, 
  Factor1       int(10), 
  Factor2       int(10), 
  Factor3       int(10), 
  Factor4       int(10), 
  Overall       int(10), 
  Discriminator varchar(255) NOT NULL, 
  created_at    timestamp NULL, 
  updated_at    timestamp NULL, 
  deleted_at    timestamp NULL, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE SiteStats (
  ID                 int(10) NOT NULL AUTO_INCREMENT, 
  OverallTimeSpent   int(10), 
  AverageTimeSpent   int(10), 
  Entries            int(10), 
  InteractivityIndex int(10), 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE TimeFrame (
  ClubID  int(10) NOT NULL, 
  ID      int(10) NOT NULL AUTO_INCREMENT, 
  Name    varchar(255), 
  `Open`  time, 
  `Close` time, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
CREATE TABLE users (
  ID                int(11) NOT NULL AUTO_INCREMENT, 
  email             varchar(255) NOT NULL UNIQUE, 
  email_verified_at timestamp NULL, 
  password          varchar(255), 
  created_at        timestamp NULL, 
  updated_at        timestamp NULL, 
  PRIMARY KEY (ID)) ENGINE=InnoDB;
ALTER TABLE Event_bands ADD CONSTRAINT FKEvent_band39979 FOREIGN KEY (EventID) REFERENCES Event (ID);
ALTER TABLE Event_ticketSites ADD CONSTRAINT FKEvent_tick246151 FOREIGN KEY (EventID) REFERENCES Event (ID);
ALTER TABLE TimeFrame ADD CONSTRAINT FKTimeFrame910043 FOREIGN KEY (ClubID) REFERENCES Club (ID);
ALTER TABLE Media ADD CONSTRAINT FKMedia275999 FOREIGN KEY (MediaLogosID) REFERENCES MediaLogos (ID);
ALTER TABLE Media ADD CONSTRAINT FKMedia771171 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Club ADD CONSTRAINT FKClub72973 FOREIGN KEY (SiteStatsID) REFERENCES SiteStats (ID);
ALTER TABLE Event ADD CONSTRAINT FKEvent837543 FOREIGN KEY (SiteStatsID) REFERENCES SiteStats (ID);
ALTER TABLE Attendance ADD CONSTRAINT FKAttendance619645 FOREIGN KEY (EventID) REFERENCES Event (ID);
ALTER TABLE Attendance ADD CONSTRAINT FKAttendance560373 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Organization ADD CONSTRAINT FKOrganizati795559 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Organization ADD CONSTRAINT FKOrganizati736287 FOREIGN KEY (EventID) REFERENCES Event (ID);
ALTER TABLE EventGenres ADD CONSTRAINT FKEventGenre993024 FOREIGN KEY (EventID) REFERENCES Event (ID);
ALTER TABLE EventGenres ADD CONSTRAINT FKEventGenre899681 FOREIGN KEY (GenreID) REFERENCES Genre (ID);
ALTER TABLE ClubGenres ADD CONSTRAINT FKClubGenres254374 FOREIGN KEY (ClubID) REFERENCES Club (ID);
ALTER TABLE ClubGenres ADD CONSTRAINT FKClubGenres242318 FOREIGN KEY (GenreID) REFERENCES Genre (ID);
ALTER TABLE Favourites ADD CONSTRAINT FKFavourites899667 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Club ADD CONSTRAINT FKClub537388 FOREIGN KEY (ClubTypeID) REFERENCES ClubType (ID);
ALTER TABLE Club ADD CONSTRAINT FKClub905672 FOREIGN KEY (CityID) REFERENCES City (ID);
ALTER TABLE AppUser ADD CONSTRAINT FKAppUser426179 FOREIGN KEY (CityID) REFERENCES City (ID);
ALTER TABLE Friendship ADD CONSTRAINT FKFriendship692483 FOREIGN KEY (AppUserID2) REFERENCES AppUser (ID);
ALTER TABLE Friendship ADD CONSTRAINT FKFriendship267837 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Review ADD CONSTRAINT FKReview991284 FOREIGN KEY (EventID) REFERENCES Event (ID);
ALTER TABLE Review ADD CONSTRAINT FKReview99655 FOREIGN KEY (ClubID) REFERENCES Club (ID);
ALTER TABLE Review ADD CONSTRAINT FKReview932012 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Club ADD CONSTRAINT FKClub655149 FOREIGN KEY (AppUserID) REFERENCES AppUser (ID);
ALTER TABLE Favourites ADD CONSTRAINT FKFavourites880055 FOREIGN KEY (GenreID) REFERENCES Genre (ID);
ALTER TABLE Event ADD CONSTRAINT FKEvent922246 FOREIGN KEY (ClubID) REFERENCES Club (ID);
ALTER TABLE AppUser ADD CONSTRAINT FKAppUser326656 FOREIGN KEY (ID) REFERENCES users (ID);

