#!/bin/bash
composer install
php artisan migrate
php artisan passport:install
php artisan key:generate
php artisan db:seed
sudo chmod -R 4755 ~/srv/clubby.me/storage
sudo chmod -R 4755 ~/srv/clubby.me/bootstrap
